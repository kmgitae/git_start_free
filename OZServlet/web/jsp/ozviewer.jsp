﻿<%@ page import="java.net.URLDecoder"%>
<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%
  
	String viewerInstallURL = request.getParameter("ozviewer");
	String viewerInstallPort = request.getParameter("port");
	
	int idx = viewerInstallURL.indexOf("/", 8);
	String startCab = viewerInstallURL.substring(0, idx);
	String cab = startCab + ":" + viewerInstallPort + viewerInstallURL.substring(idx);
	
	String ozServer = request.getParameter("ozserver");
	String strReportname = request.getParameter("reportname");
 
	
	String strExt = request.getParameter("type");

%>
<html>
<head>

    <script language = "JavaScript" src = "<%=cab%>/ztransferx_browers.js"></script>
    <script language = "JavaScript" src = "<%=cab%>/ozviewer_browers.js"></script>
    <script language ="JavaScript">
        function ZTInstallEndCommand_ZTransferX(param1,param2) {

			

            Create_Div();
            Initialize_OZViewer("OZReportViewer", "CLSID:0DEF32F8-170F-46f8-B1FF-4BF7443F5F25", "100%", "100%", "application/OZRViewer");
            Insert_OZViewer_Param("connection.servlet", "<%=ozServer%>");
            Insert_OZViewer_Param("viewer.isframe", "<%=request.getParameter("isframe")%>");
            Insert_OZViewer_Param("viewer.namespace", "OZReportDemo/ozviewer");
			Insert_OZViewer_Param("viewer.configmode", "html");
			Insert_OZViewer_Param("viewer.bgcolor", "ffffff");
			Insert_OZViewer_Param("viewer.smartframesize", "true");
			Insert_OZViewer_Param("toolbar.viewmode", "true");
			Insert_OZViewer_Param("toolbar.position", "top");
			Insert_OZViewer_Param("global.language", "ko/KR");
			Insert_OZViewer_Param("information.debug", "true");
			Insert_OZViewer_Param("viewer.viewmode","fittowidth");
			
			
			<%if(strExt.compareTo("ozr")==0){%>
				Insert_OZViewer_Param("connection.reportname", "<%=strReportname%>");
				Insert_OZViewer_Param("viewer.showtree", "false");
				Insert_OZViewer_Param("connection.compresseddatamodule", "true");
				Insert_OZViewer_Param("connection.compressedForm", "true");
			<%}else{  %>
				Insert_OZViewer_Param("connection.openfile", "ozp://<%=strReportname%>");
			<%} %>
			
            Start_OZViewer();
        }
    </script>
</head>

<body>
<div id ="InstallOZViewer">
    <script language = "JavaScript">
		Initialize_ZT("ZTransferX", "CLSID:C7C7225A-9476-47AC-B0B0-FF3B79D55E67", "0", "0", "<%=cab%>/ZTransferX_2,2,3,8.cab#version=2,2,3,8");
		Insert_ZT_Param("download.server", "<%=viewerInstallURL%>");
        Insert_ZT_Param("download.port", "<%=viewerInstallPort%>");
        Insert_ZT_Param("download.instruction", "ozrviewer.idf");
        Insert_ZT_Param("install.base", "<PROGRAMS>/Forcs");
        Insert_ZT_Param("install.namespace", "OZReportDemo");
		
        Start_ZT('<%=cab%>');
    </script>
</div>
</body>
</html>