﻿<%
	String ozServer = request.getParameter("ozserver");
	String strReportname = request.getParameter("reportname");
	
	String strExt = request.getParameter("type");

%>

<head>
<meta http-equiv="Content-Type" content="text/html  charset=utf-8"/>
<script src="../../FlashViewer/AC_OETags.js"	language="javascript"></script>
<script src="../../FlashViewer/ozutil.js"		language="javascript"></script>
<script src="../../FlashViewer/ozjscript.js"	language="javascript"></script>
<script language="javascript"	type="text/javascript">
var requiredMajorVersion	= 9;
var requiredMinorVersion	= 0;
var requiredRevision		= 0;
</script>
</head>

<body BOTTOMMARGIN="0" SCROLLING="no" TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINHEIGHT="0" MARGINWIDTH="0" BGCOLOR="white">

<script language="JavaScript" type="text/javascript">
var hasProductInstall		= DetectFlashVer(6, 0, 65);
var hasRequestedVersion		= DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

if ( hasProductInstall && !hasRequestedVersion ) {
  var MMPlayerType	= ( isIE == true ) ? "ActiveX" : "PlugIn";
	var MMredirectURL	= window.location;
	document.title		= document.title.slice(0, 47) + " - Flash Player Installation";
	var MMdoctitle		= document.title;
	
	AC_FL_RunContent (
		"src"			,"playerProductInstall",
		"FlashVars"		,"MMredirectURL="+MMredirectURL+'&MMplayerType='+MMPlayerType+'&MMdoctitle='+MMdoctitle+"",
		"width"			,"100%",
		"height"		,"100%",
		"align"			,"middle",
		"id"			,"playerProductInstall",
		"quality"		,"high",
		"bgcolor"		,"#ffffff",
		"name"			,"playerProductInstall",
		"allowScriptAccess"	,"sameDomain",
		"type"			,"application/x-shockwave-flash",
		"pluginspage"		,"http://www.adobe.com/go/getflashplayer"
	);
} else if ( hasRequestedVersion ) {
	function SetOZParamters_OZViewer() {
	  var oz;
		if ( navigator.appName.indexOf("Microsoft") != -1 ) {
			oz = window["OZViewer"];
		} else {
			oz = document["OZViewer"];
		}
		
	oz.sendToActionScript("connection.servlet", "<%=ozServer%>");
<%if(strExt.compareTo("ozr")==0){%>
	oz.sendToActionScript("connection.reportname", "<%=strReportname%>");
<%}else{  %>
	oz.sendToActionScript("connection.openfile", "ozp://<%=strReportname%>");
<%} %>
	oz.sendToActionScript("viewer.focus_doc_index", "0");
    oz.sendToActionScript("viewer.viewmode","fittowidth");
	oz.sendToActionScript("print.alldocument", "true");
	oz.sendToActionScript("global.concatpage", "true");
//	oz.sendToActionScript("export.applyformat", "ozd");
	oz.sendToActionScript("viewer.smartframesize", "true");

	oz.sendToActionScript("toolbar.viewmode", "true");
	oz.sendToActionScript("global.language", "ko/KR");

	oz.sendToActionScript("toolbar.comment","true");
	oz.sendToActionScript("comment.all","true");
	oz.sendToActionScript("toolbar.print","true");

	oz.sendToActionScript("information.debug","true");

	
	return true;
}
	
	function OZProgressCommand_OZViewer(step, state, reportname)
	{
	}
	
	AC_FL_RunContent (
		"src"			,"FlashViewer/OZViewer"+_OZ_versionMajor,
		"width"			,"100%",
		"height"		,"100%",
		"align"			,"middle",
		"id"			,"OZViewer",
		"quality"		,"high",
		"bgcolor"		,"#ffffff",
		"name"			,"OZViewer",
		"allowScriptAccess"	,"sameDomain",
		"type"			,"application/x-shockwave-flash",
		"pluginspage"		,"http://www.adobe.com/go/getflashplayer",
		"flashVars"		,"flash.objectid=OZViewer"
	);
	
} else {
		var alternateContent	= 'Alternate HTML content should be placed here. '
					+ 'This content requires the Adobe Flash Player. '
					+ '<a href=http://www.adobe.com/go/getflash/>Get Flash</a>';
		document.write(alternateContent);
}

</script>
</body>
