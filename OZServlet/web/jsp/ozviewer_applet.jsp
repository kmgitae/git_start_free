﻿<html>
<head>
<%
	
	String ozServer = request.getParameter("ozserver");
	String strReportname = request.getParameter("reportname");
	
	String strExt = request.getParameter("type");
	String ozVer = request.getParameter("ozver");

%>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body topmargin="0" leftmargin="0" scroll="no">
<script src="http://java.com/js/deployJava.js"></script>
<script src="../../AppletViewer/deployOZ.js"></script>
<script language="JavaScript">

function OZExportCommand_ozviewer(code, path, filename, step) {}
function OZCommand_ozviewer(code, args) {}
function OZUserActionCommand_ozviewer(type, attr) {}

function getOZApplet() {
  var oz;
	if(navigator.appName.indexOf("Microsoft") != -1){
		oz = window[OZViewerName];
	}else{
		oz = document[OZViewerName];
	}
	return oz;
}

var OZViewerName = "ozviewer"; 
var javaVersion = "1.7.5"; 
var appletParameter = new Array (
  "id="+OZViewerName,
	"width=100%", 
	"height=100%",
  "codebase=../../AppletViewer/oz/applet/", 
  "ozversion=<%=ozVer%>" 
);

		
var viewerParameter = new Array (
  "connection.servlet=<%=ozServer%>",
<%if(strExt.compareTo("ozr")==0){%>
  "connection.reportname=<%=strReportname%>",
<%}else{  %>
  "connection.openfile=ozp://<%=strReportname%>",
<%} %>
	"viewer.configmode=html",
	"viewer.isframe=false",
	"toolbar.viewmode=true",
//	"viewer.focus_doc_index=0",
	"print.alldocument=true",
	"global.concatpage=true",
    "viewer.smartframesize=true",
	"connection.compresseddatamodule=true",
	"connection.compressedForm=true",
	"global.language=ko/KR"
);
                    
OZAppletViewerStart(appletParameter, viewerParameter, javaVersion);               
</script>
</body>
</html>
