﻿<%@ page import="sample.RepositoryTest"%>
<%@ page import="java.net.URLDecoder"%>
<%@page import="java.net.URLEncoder"%><%

//--------------------------------------------------------------------------
//Creator by ozdev. ssj ----------------------------------------------------
//Init Setting 
//--------------------------------------------------------------------------
String ozServer = "http://211.116.251.249:8080/OZServlet/server"; //OZServer path.
String ozrv = "http://211.116.251.249/OZServlet/ActiveXViewer"; //OZViewer ZTransfer Install path.
String port = "8080"; //OZViewer ZTransfer Install path port
String id = "admin"; //OZServer ID
String pwd = "admin"; //OZServer PWD
String category = "/Eform"; //OZServer Repository ROOT category

String appletVer = "60.2015.0114.200"; //OZApplet Viewer Version
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------



String popup = "";
String vType = "ActiveX";
//String vType = "Flash";



RepositoryTest t = new RepositoryTest(ozServer, id, pwd, request.getParameter("type"), category, "name");
String body = t.getTag();

%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0; maximum-scale=0.75; minimum-scale=1.0; user-scalable=1;"/>
	<!--link rel="stylesheet"  href="jquery.mobile-1.0rc2.min.css" /-->
	<link rel="stylesheet"  href="../docs/_assets/css/jquery.mobile-1.0rc2.css" />
	<link rel="stylesheet" href="../docs/_assets/css/jqm-docs.css" />
	<script src="../docs/_assets/js/jquery-1.6.4.min.js"></script>
	<script src="../docs/_assets/js/jquery.mobile-1.3.1.js"></script>

	
<script language="javascript"> 

ozurl = "";

function OZRefresh()
{

	if (ozurl == "") return;
 	parent.ozbody.location.href = getLocation() + ozurl;
}

function OZRForm(reportname)
{
	
	ozurl = "&type=ozr&reportname="+reportname;
	OZRefresh();
}

function OZDForm(reportname)
{
	ozurl = "&type=ozd&reportname="+reportname;
	OZRefresh();
}

function getLocation()
{
	var vType = document.getElementById('vvvType').innerHTML;
	
	var location = "ozviewer.jsp?isframe=false&ozviewer=<%=ozrv%>&port=<%=port%>"; //ActiveX
	if (vType == "Applet")
	{

		location = "ozviewer_applet.jsp?isframe=false&ozver=<%=appletVer%>";
		
	}else if (vType == "Flash")
	{

		location = "ozviewer_flash.jsp?isframe=false";
		
	}else if (vType == "HTML5")
	{

		location = "ozviewer_html5.jsp?isframe=false";	

	}	else if (vType == "ActiveX (Popup)")
	{

		location = "ozviewer.jsp?isframe=true&ozviewer=<%=ozrv%>&port=<%=port%>";
		
	}
	location = location + "&ozserver=<%=ozServer%>";

	return location;
}

</script>	
	
</head>
<div id=vType>
<body> 
	<div data-role="page" class="type-index">

		<div data-role="header" data-theme="b">
				<table width=100% height=50 border=0 CELLPADDING="0" CELLSPACING="0"><tr><td align="center" background="images/bg04.gif"><font size=2><div name=vvvType id=vvvType><%=vType%> <%=popup%></div></font></td></tr></table>
		</div><!-- /header -->	

		<div data-role='content'>
			<%=body%>
		</div>
		
		<div data-role="footer" class="footer-docs" data-theme="c">
			<p align="right"><font style="font-size:9pt;">Copyright &copy; 2015 FORCS All rights reserved.</font></p>
		</div>	
	
	</div>
</body>
</html>
