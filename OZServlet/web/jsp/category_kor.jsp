﻿<%@ page contentType="text/html; charset=utf-8" import="sample.RepositoryTest"%>
<%@ include file="global_category_kor.jsp" %>
<%
RepositoryTest t = new RepositoryTest(ozServer, id, pwd, "comment", category, itemDisplayType);
String body = t.getTag();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--<meta name="viewport" content="initial-scale=1.0; maximum-scale=0.75; minimum-scale=1.0; user-scalable=1; width=device-width;"/> -->
<meta name="viewport" content="initial-scale=2.0; maximum-scale=1.0; minimum-scale=1.0; user-scalable=no; width=device-width;"/>
<title>OZ Mobile</title>
<link rel="stylesheet" href="../css/themes/default/jquery.mobile-1.1.0.css" />
<link rel="stylesheet" href="../docs/_assets/css/jqm-docs.css" />
<script src="../js/jquery.js"></script>
<script src="../js/jquery.mobile-1.1.0.js"></script>
<script language="javascript"> 

function OZUserActionCommand_ozviewer(type, attrs) {}

function OZProgressCommand_ozviewer(step, state, reportname) {}

function OZPrintCommand_ozviewer(msg, code, reportname, printername, printcopy, printpages, printrange, username, printerdrivername, printpagesrange) { }
			
function OZPostCommand_ozviewer(cmd, msg) {}
			
function OZMailCommand_ozviewer(code) {}
			
function OZLinkCommand_ozviewer(docIndex, compName, tag, value) {}
			
function OZExportCommand_ozviewer(code, path, filename, pagecount, filenames) {}
			
function OZExitCommand_ozviewer() {}
			
function OZErrorCommand_ozviewer(code, errmsg, detailmsg, reportname) {}
			
function OZCommand_ozviewer(code, args) {}
			
function OZBankBookPrintCommand_ozviewer(data) {}

function OZRForm(reportname)
{
	var c = reportname.substring(0, reportname.lastIndexOf("/"));
	var r = reportname.substring(reportname.lastIndexOf("/")+1);
	while (c.indexOf("/") > -1) {
		c = c.replace("/", "_");
	}
	r = r.replace(".ozr", "");
	var rst = document.getElementById(c + "_" + r).value;
	if (rst > 0)
		location.href = "mobile-ozviewer.jsp?reportname=" + reportname;
	else {
		var p = new Array();
		var param = "";

		p[p.length] = "connection.servlet=<%=ozServer%>";
		p[p.length] = "connection.reportname=" + reportname;
		p[p.length] = "information.debug=true";
		p[p.length] = "eform.signpad_type=zoom";
		p[p.length] = "comment.all=true";
		p[p.length] = "toolbar.all=true";
		p[p.length] = "eform.signpad_zoom=600";
		p[p.length] = "viewer.viewmode=fittowidth";
		p[p.length] = "viewer.pagedisplay=singlepage";
		p[p.length] = "font.fontnames=font1,font2";
		p[p.length] = "font.font1.url=http://www.forcs.com/font/malgun.ttf";
		p[p.length] = "font.font1.name=맑은 고딕";
		p[p.length] = "font.font2.url=http://www.forcs.com/font/BareunDotum1.ttf";
		p[p.length] = "font.font2.name=바른돋움 1";

		param = p.join("&");
		
	    location.href = "http://mobile.viewer.forcs.com?" + param;
	}
}

function OZDForm(reportname)
{
	var p = new Array();
	var param = "";

	p[p.length] = "connection.servlet=<%=ozServer%>";
	p[p.length] = "connection.openfile=ozp://" + reportname;
	p[p.length] = "information.debug=true";
	p[p.length] = "eform.signpad_type=zoom";
	p[p.length] = "comment.all=true";
	p[p.length] = "toolbar.all=true";
	p[p.length] = "eform.signpad_zoom=600";
	p[p.length] = "viewer.viewmode=fittowidth";
	p[p.length] = "viewer.pagedisplay=singlepage";

	param = p.join("&");
	
	location.href = "http://mobile.viewer.forcs.com?" + param;
}

function getOZUrlType()
{
  var matchDatas = document.location.href.match(/ozurltype=(\w+)/);
  if(matchDatas == null)
    return "protocol";
  else
    return matchDatas[1];
}

</script>	
	<!--
	var p = new Array();
	var param = "";

	p[p.length] = "connection.servlet=<%=ozServer%>";
	p[p.length] = "connection.reportname=" + reportname;
	p[p.length] = "information.debug=true";
	p[p.length] = "global.language=en/us";
	
	param = p.join("&");
	
	location.href = "ozr://ozviewer?" + param;
-->
</head> 
<body> 

	<div data-role="page" class="type-index">

		<div data-role="header" data-theme="c" >
			<!--<a href="index.html" data-icon="search" data-theme="d" class="ui-btn-right"></a> -->
			<h1>OZ Demo list</h1>
			
		</div><!-- /header -->
		
		<div id="jqm-homeheader" >
			<h1>Demo Category</h1>
			<h2>업종별 샘플</h2>
			
		</div>		

		<div data-role='content' data-theme="c">
		<%=body%>
		</div>




		<div id = "jqm-homefooter" data-role="footer" class="footer-docs" data-theme="g" >
			<p align=center>Copyright &copy; 2015 FORCS All rights reserved.</p>
			<p align=center>데모 관련 문의 thunder3@forcs.com</p>
		</div>	
	
	</div>
	
</body>
</html>
