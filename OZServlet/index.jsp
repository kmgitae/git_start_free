﻿<!--DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"-->
<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%

String vType = request.getParameter("vType");

if (vType == null || "".equals(vType))
{
	//vType = "ActiveX";
	vType = "Flash";
}

String isFrame = request.getParameter("isframe");
if (isFrame == null || "".equals(isFrame))
{
	isFrame = "false";
}


%>
<html>
<head>
	<title>OZ Demo Page New</title>
	<meta name="description" content="This Web page is for OZ Demonstration.">
	<meta name="keywords" content="OZ Solution">
	<meta name="robots" content="index,follow">
</head>
<frameset rows="65,*" frameborder="0">
	<frame src="web/jsp/top.jsp" name="top" scrolling="no" marginwidth="0" marginheight="0" frameborder="0">
		<frameset cols="250,*" border="0">
			<frame src="web/jsp/tree.jsp?type=comment&vType=<%=vType%>&isframe=<%=isFrame%>" name="tree" id="tree" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" noresize>
			<frame src="web/jsp/intro.jsp" name="ozbody" id="ozbody" frameborder="0" scrolling="no"></frame>
		</frameset>
	</frame>
</frameset>
<body>
</body>
</html>
