function GetOZDataModule()
{
	var dm = _GetNewDataModule();
	var ozdata = _GetParamValue("ozdata");
	var odiname = _GetParamValue("odiname");

	var odi_param = "";
	var DS = ozdata.split("//OZDATASET//"); //데이터셋 구분자(//OZDATASET//)
	dm.ParseData(odiname, DS, odi_param, "//OZRecord//", ";"); //줄구분자(//OZRecord//)//컬럼구분자^
	return dm;
}