<%@ page contentType="text/html;charset=UTF-8" 
%><%@ page import="com.oreilly.servlet.MultipartRequest,
                   com.oreilly.servlet.multipart.DefaultFileRenamePolicy,
                   java.util.*" 
%><%
 String savePath="C:/Tomcat7/webapps/OZServlet/upload_doc"; // 저장할 디렉토리 (절대경로)

 int sizeLimit = 50 * 1024 * 1024 ; // 5메가까지 제한 넘어서면 예외발생

 try{

	MultipartRequest multi=new MultipartRequest(request, savePath, sizeLimit, new DefaultFileRenamePolicy());
 	Enumeration formNames=multi.getFileNames();  // 폼의 이름 반환
	String formName=(String)formNames.nextElement(); // 자료가 많을 경우엔 while 문을 사용
	String fileName=multi.getFilesystemName(formName); // 파일의 이름 얻기

	if(fileName == null) {   // 파일이 업로드 되지 않았을때
		out.print("File name is not upload.");
	} else {  // 파일이 업로드 되었을때
		fileName=new String(fileName.getBytes("8859_1"),"UTF-8"); // 한글인코딩 - 브라우져에 출력
		out.print("User Name : " + multi.getParameter("userName") + "<BR>");
		out.print("File Name  : " + fileName);
	} // end if

 } catch(Exception e) {
	out.print("error : "+e.getMessage());
 }
%>
